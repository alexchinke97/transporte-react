import React, { useEffect, useRef } from 'react'
import './index.css';

interface Props{
    show:boolean;
    onDismiss(e:boolean):void;
}

const CustomModal: React.FC<Props> = ({show,onDismiss,children}) => {

    const refModal = useRef<HTMLDivElement>(null);

    useEffect(()=>{
        if(show){
            refModal.current!.style.display = "block";
        }else{
            refModal.current!.style.display = "none";
        }
    },[show])

    return (
        <div ref={refModal} id="myModal" className="modal">

            <div className="modal-content">
                <span onClick={()=>onDismiss(false)}  className="close">&times;</span>
                {children}
            </div>

        </div>
    )
}

export default CustomModal;