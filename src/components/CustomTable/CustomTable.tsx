import React, { ReactNode } from 'react';
import './index.css';

interface Props {
    header: string[];
    cells?: Object[]
}

const CustomTable: React.FC<Props> = ({ header, cells, children }) => {
    return (
        <table>
            <thead>
                <tr>
                    {header.map((item, index) => <th key={index}>{item}</th>)}
                </tr>
            </thead>
            <tbody>
                {cells?.map((item) => <tr>{Object.values(item).map(field => <td>{field}</td>)}</tr>)}
                {children && children}
            </tbody>
        </table>
    )
}

export default CustomTable;