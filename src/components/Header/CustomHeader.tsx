import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import './index.css';

interface Props{
    title: string;
}
const CustomHeader:React.FC<Props> = ({title})=>{
    const history = useHistory();
    const goHome = ()=>{
        history.push("/main");
    }
    return(
        <div className="header">
             <span className="title">{title}</span>
             <span style={myStyle} onClick={goHome}>Ir a inicio</span>
            <Link style={myStyle} to="/empleados">Ir a empleados</Link>
        </div>
    )
}

const myStyle: React.CSSProperties = {
    marginRight: 20, 
    textDecoration: "underline", 
    color: "white",
    cursor: "pointer"
}
export default CustomHeader;