

//Constantes para definir las acciones

import { getSuggestedQuery } from "@testing-library/react";
import axios from "axios";
import { Dispatch } from "hoist-non-react-statics/node_modules/@types/react";
import { EmpleadoModel } from "../models/Empleados";
import { ApiService } from "../service/api.service";

const OBTENER_EMPLEADOS = "OBTENER_EMPLEADOS";
const OBTENER_EMPLEADO = "OBTENER_EMPLEADO";
const ACTUALIZAR_EMPLEADO = "ACTUALIZAR_EMPLEADO";
const ELIMINAR_EMPLEADO = "ELIMINAR_EMPLEADO";

//State type



interface EmpleadoStateType{
    empleados: EmpleadoModel[]
}

//Actions Types

interface ObtenerEmpleadosActionType{
    type: typeof OBTENER_EMPLEADOS;
    payload: EmpleadoStateType;
}

type EmpleadoAction = ObtenerEmpleadosActionType;


const initialState: EmpleadoStateType = {
    empleados:[]
}

// Reducer

export const EmpleadoReducer = (state = initialState, action: EmpleadoAction)=>{
    switch(action.type){
        case OBTENER_EMPLEADOS:{
            return{...state,...action.payload}
        }
        default: return state;
    }
}

export const ObtenerEmpleadosAction = () => async (distpatch: Dispatch<EmpleadoAction>)=>{
    try{
        const resp = await ApiService.GetAllEmployees();  
        console.log(resp.data.data)
        distpatch(
            {
                type: OBTENER_EMPLEADOS, 
                payload:{
                    empleados: resp.data.data
                }
            }
        )      
    }catch(e){
        distpatch(
            {
                type: OBTENER_EMPLEADOS, 
                payload:{
                    empleados: []
                }
            }
        ) 
    }
}

