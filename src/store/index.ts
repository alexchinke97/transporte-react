import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import { EmpleadoReducer } from './Empleado';

const rootReducer = combineReducers({
    Empleados:EmpleadoReducer
})

export const rootStore = createStore(rootReducer, applyMiddleware(thunk));

export type RootStore = ReturnType<typeof rootReducer>;
