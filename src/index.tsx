import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './App';
import { rootStore } from './store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={rootStore}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);


/**
 * Temas que se ven en este CRUD (React Typescript)
 * 
 * Las Props => 
 *  en los componentes /Header/CustomTable/Modal
 *  pasandolo a su componente por medio de interfaces
 * 
 * Redux => 
 *  en la carpeta store esta el setup y su reducer
 *  aplicando el patron del "patito" (Duck)
 * 
 * Navegacion con React Router =>
 *  se puede ver la estructura del ruteo en App.tsx
 *  tambien se muestra en el componente Header la navegacion
 *  por medio de etiquetas <Link to="/empleados"> y por
 *  codigo con el Hook useHistory()
 * 
 * 
 * El hook useEffect => 
 *  se puede encontrar tanto en los componentes como en el
 *  pages/EmpleadoPage.tsx 
 * 
 * Servicio de Api => 
 *  se puede ver en la clase ApiService usando axios para
 *  hacer peticiones a la api
 * 
 * Inteface =>
 *  en la carpeta models se puede encontrar todas las entidades
 *  por "mapear", notar que algunas de sus propiedades
 *  pueden ser opcionales
 *  
 * 
 *
 * 
 */
