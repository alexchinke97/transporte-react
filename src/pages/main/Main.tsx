import React from 'react';
import {Link, useHistory} from 'react-router-dom';
import CustomHeader from '../../components/Header/CustomHeader';
import './Main.css';

const MainPage:React.FC = () => {
    return (
        <div>
            <CustomHeader title="Inicio" />
            <br/><br/>
            <h2>Este es el inicio</h2>
        </div>
    );
}


export default MainPage;