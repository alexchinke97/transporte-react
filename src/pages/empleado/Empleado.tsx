import React, { ChangeEventHandler, FormEventHandler, useEffect, useState } from 'react';
import CustomHeader from '../../components/Header/CustomHeader';
import CustomTable from '../../components/CustomTable/CustomTable';
import { connect, useDispatch } from 'react-redux';
import { RootStore } from '../../store';
import { ObtenerEmpleadosAction } from '../../store/Empleado';
import CustomModal from '../../components/Modal/CustomModal';
import { EmpleadoModel } from '../../models/Empleados';
import { ApiService } from '../../service/api.service';

const header = [
    "DUI",
    "NOMBRES",
    "APELLIDOS",
    "FECHA NAC",
    "ESTADO CIVIL",
    "TEL",
    "TIPO",
    "PATH IMAGE"
]
const EmpleadoPage: React.FC<RootStore> = ({ Empleados }) => {
    const [openModal,setOpenModal] = useState(false);
    const [selected, setSeleted] = useState<EmpleadoModel>();    
    const dispatch = useDispatch();


    const setEmployee = (e:EmpleadoModel)=>{
        console.log(e);
        setSeleted(e);
    }



    const CustomFields = (e:EmpleadoModel) => {
        return (
            <td>
                <button onClick={()=>{
                    setOpenModal(true); 
                    setEmployee(e)
                }}>Editar</button><br/>
                <button>Eliminar</button>
            </td>
        )
    }

    const handleSubmit:FormEventHandler<HTMLFormElement> = (e)=>{
        e.preventDefault();
        console.log(e);
        Actualizar();
    }

    const handleChange:ChangeEventHandler<HTMLInputElement> = (e)=>{
        const {name,value} = e.target;
        if(selected){
            setSeleted({
                ...selected,
                [name]: value
            }) 
        }
    }

    const Actualizar = async ()=>{
        if(selected){
            ApiService.UpdateEmployee(selected).then(resp=>{
                console.log(resp);
                let {code, msg}  = resp.data;
                code = 1 && alert(msg);
                code = 0 && alert(msg);
                dispatch(ObtenerEmpleadosAction());
                setOpenModal(false);
            }).catch(e=>{
                alert(e.msg);
            })
        }
    }

    useEffect(() => {
        dispatch(ObtenerEmpleadosAction())
    }, [])
    return (
        <div>
            <CustomHeader title="Empleado" />
            <br />
            <CustomTable header={header} >
                {Empleados.empleados.map(item => (
                    <tr>
                        {Object.values(item).map(field => <td>{field}</td>)}
                        {CustomFields(item)}
                    </tr>
                ))}
            </CustomTable>
            <CustomModal show={openModal} onDismiss={(e)=>{setOpenModal(e)}}>
                <br/>
                <h3>Editar Empleado</h3>
                <form onSubmit={handleSubmit}>
                    DUI:
                    <input 
                            type="text" 
                            name="dui"
                            value={selected?.dui}
                            onChange={handleChange}
                            disabled={true}
                             />
                    <br/><br/>
                    NOMBRES:
                    <input 
                        type="text" 
                        name="nombres"
                        value={selected?.nombres}
                        onChange={handleChange} />
                    APELLIDOS:
                    <input 
                        type="text" 
                        name="apellidos"
                        value={selected?.apellidos}
                        onChange={handleChange} />
                    <br/><br/>
                    FECHA NACIMIENTO:
                    <input 
                        type="date" 
                        name="fechanacimiento"
                        value={selected?.fechanacimiento.toString()}
                        onChange={handleChange} />
                    TELEFONO:
                    <input 
                        type="text" 
                        name="telefono"
                        value={selected?.telefono}
                        onChange={handleChange} />
                    <br/><br/>
                    <button>GUARDAR</button>
                </form>
            </CustomModal>
        </div>
    );
}


const mapStateToProps = (state: RootStore) => state;
export default connect(mapStateToProps)(EmpleadoPage);