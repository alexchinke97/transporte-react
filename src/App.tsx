import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import MainPage from './pages/main/Main';
import EmpleadoPage from './pages/empleado/Empleado';
const App:React.FC = ()=> {
  

  return (
    <Router>
      <Switch>
      <Route path="/empleados">
          <EmpleadoPage />
        </Route>
        <Route path="/main">
          <MainPage />
        </Route>
        <Route path="/">
          <MainPage />
        </Route>
      </Switch>
  </Router>
  );
}



export default App;
