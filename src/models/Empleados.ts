export interface EmpleadoModel{
    dui:string;
    nombres:string;
    apellidos:string;
    fechanacimiento:Date;
    estadocivil:string;
    telefono:string;
    tipo:string;
    avatarpath:string;
    estadoregistro:boolean;
}