import axios from 'axios';
import { EmpleadoModel } from '../models/Empleados';

export class ApiService{
    static GetAllEmployees(){
        return axios.get('http://localhost:8080/api/empleado');
    }

    static UpdateEmployee(e: EmpleadoModel){
        const {dui} = e;
        return axios.put(`http://localhost:8080/api/empleado/${dui}`, e);
    }
}